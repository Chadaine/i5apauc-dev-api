<!DOCTYPE html>
<html lang='fr-FR'>
    <head>
        <title>Résultats F1</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="style.css">
        <link rel="icon" href="Images/F1-Logo.PNG">
    </head>
    <body>
        <?php
			ini_set('display_errors', 0);
            $url = "http://ergast.com/api/f1/".$_POST['year']."/".$_POST['round']."/results.json";

            // Initialise la session cURL
            $curl = curl_init();

            // Définit les options cURL
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);

            // Exécute la requête
            $data = curl_exec($curl);
            $data = json_decode($data,true);
            curl_close($curl);

            // On initialise un tableau pour stocker les données de l'API
            $results = array();
            $GPname = $data['MRData']['RaceTable']['Races'][0]['raceName'];
            // On parcours les données de l'API pour récupérer les informations des courses
            foreach ($data['MRData']['RaceTable']['Races']['0']['Results'] as $resultData) {
                $results[] = array(
                    'position' => $resultData['position'],
                    'number' => $resultData['Driver']['permanentNumber'],
                    'firstName' => $resultData['Driver']['givenName'],
                    'lastName' => $resultData['Driver']['familyName'],
                    'constructor' => $resultData['Constructor']['name'],
                    'time' => $resultData['Time']['time'],
                    'points' => $resultData['points']
                );
            }
        ?>
        <div class="resultTable">
            <table>
                <tr>
                    <th>Position</th>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Constructor</th>
                    <th>Time</th>
                    <th>Points</th>
                </tr>
                <?php
                    echo '<h1> Résultats de course : '.$GPname.' '. $_POST['year'] .'</h1>';
                    foreach ($results as $result) {
                        echo '<tr>';
                        echo '<td><h2 class="_'.$result['position'].'">Position : '.$result['position'].'</h2></td>';
                        echo '<td><h3>#'.$result['number'].'</h3></td>';
                        echo '<td><h3>'.$result['firstName'].' '.$result['lastName'].'</h3></td>';
                        echo '<td><h3>'.$result['constructor'].'</h3></td>';
                        echo '<td><h3>'.$result['time'].'</h3></td>';
                        echo '<td><h3>'.$result['points'].'</h3></td>';
                        echo '</tr>';
                    }
                ?>
            </table>        
        </div>
    </body>
</html>