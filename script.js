var minYear = 1950
var maxYear = new Date().getFullYear();
var currentDate = new Date();

$(document).ready(function () {
    var year = maxYear;
    document.getElementById("calendarTitle").innerHTML = "Calendrier des courses F1 " + year
    document.getElementById("NextYear").style.visibility="hidden";

    getData(year);

    $("#PrevYear").click(function (event) {
        event.preventDefault();

        var year = document.getElementById('calendarTitle').textContent;
        year = Number(year.substring(year.length - 4, year.length)) - 1;
        document.getElementById("calendarTitle").innerHTML = "Calendrier des courses F1 " + year;

        if (year == minYear){
            document.getElementById("PrevYear").style.visibility="hidden";
        }
        else{
            document.getElementById("PrevYear").style.visibility="visible";
            document.getElementById("NextYear").style.visibility="visible";
        }

        getData(year);
    });

    $("#NextYear").click(function (event) {
        event.preventDefault();

        var year = document.getElementById('calendarTitle').textContent;
        year = Number(year.substring(year.length - 4, year.length)) + 1
        document.getElementById("calendarTitle").innerHTML = "Calendrier des courses F1 " + year;

        if (year == maxYear){
            document.getElementById("NextYear").style.visibility="hidden";
        }
        else{
            document.getElementById("PrevYear").style.visibility="visible";
            document.getElementById("NextYear").style.visibility="visible";
        }

        getData(year);
    });

});

function getData(year) {
    apiURL = "http://ergast.com/api/f1/" + year + '.json';
    $("#races").html("");
    
    $.getJSON(apiURL, function(data) {
        const raceDiv = document.getElementById("races");

        for (let i = 0; i<data.MRData.RaceTable.Races.length; i++){
            // Background
            const bgDiv = document.createElement("div");
            bgDiv.style.background = 'url("Images/Flags/' + data.MRData.RaceTable.Races[i].Circuit.Location.country.replace(/\s+/g, '') + '.png")';
            bgDiv.classList.add("race-bg");
            raceDiv.appendChild(bgDiv);

            // Content div
            const contentDiv = document.createElement("div");
            contentDiv.classList.add("race-content");
            raceDiv.appendChild(contentDiv);

            // Race name
            const name = document.createElement("h1");
            name.innerHTML = "Nom de la course : " + data.MRData.RaceTable.Races[i].raceName + "<br><br>";
            contentDiv.appendChild(name);

            // Date
            const RDate = document.createElement("h2");
            RDate.innerHTML = "Date de la course : " + data.MRData.RaceTable.Races[i].date;
            contentDiv.appendChild(RDate);

            // Circuit
            const RCircuit = document.createElement("h2");
            RCircuit.innerHTML = "Nom du circuit : " + data.MRData.RaceTable.Races[i].Circuit.circuitName + "<br><br>";
            contentDiv.appendChild(RCircuit);

            // Form race results
            const raceForm = document.createElement("form");
            raceForm.method = "post";
            raceForm.action = "racewinner.php";
            contentDiv.appendChild(raceForm);
            
            // Form: Hidden value -> round number
            const hiddenVal = document.createElement("input");
            hiddenVal.type = "hidden";
            hiddenVal.name = "round";
            hiddenVal.value = data.MRData.RaceTable.Races[i].round;
            raceForm.appendChild(hiddenVal);

            // Form: Hidden value -> Year
            const hiddenVal2 = document.createElement("input");
            hiddenVal2.type = "hidden";
            hiddenVal2.name = "year";
            hiddenVal2.value = year;
            raceForm.appendChild(hiddenVal2);

            // Form: result button
            raceDate = new Date(data.MRData.RaceTable.Races[0].date);
            if (currentDate > raceDate)
            {
                console.log(currentDate <= raceDate);
                const submitResults = document.createElement("input");
                submitResults.type = "submit";
                submitResults.value = "Résultats de course";
                submitResults.classList.add("f1-button");
                raceForm.appendChild(submitResults);
            }
            else
            {
                const noResult = document.createElement("h2");
                noResult.innerHTML = "La course n'a pas encore eu lieu.";
                contentDiv.appendChild(noResult);
            }

            //console.log(data.MRData.RaceTable.Races[i].Circuit.Location.country);
        }
    });
};